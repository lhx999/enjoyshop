package com.enjoyshop.adapter;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.enjoyshop.R;

import java.util.List;

/**
 * 版权：易金卡沃 版权所有
 * <p>
 * 作者：liushijiang
 * <p>
 * 创建日期：2018/11/28
 * <p>
 * 描述：
 * <p>
 * 修订历史：
 */
public class AdapterDiscount extends BaseQuickAdapter<String, BaseViewHolder> {
    Context context;

    public AdapterDiscount(@Nullable List<String> data, Context context) {
        super(R.layout.item_discount, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {

    }
}
