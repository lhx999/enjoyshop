package com.enjoyshop.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.enjoyshop.R;
import com.enjoyshop.bean.HotGoods;
import com.enjoyshop.helper.SharePresenter;
import com.enjoyshop.utils.CartShopProvider;
import com.enjoyshop.utils.GlideUtils;
import com.enjoyshop.utils.LogUtil;
import com.enjoyshop.utils.ToastUtils;
import com.enjoyshop.widget.EnjoyshopToolBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 版权：易金卡沃 版权所有
 * <p>
 * 作者：liushijiang
 * <p>
 * 创建日期：2018/11/28
 * <p>
 * 描述：
 * <p>
 * 修订历史：
 */
public class DetailActivity extends BaseActivity {
    @BindView(R.id.id_detail_img)
    ImageView idDetailImg;
    @BindView(R.id.id_detail_title)
    TextView idDetailTitle;
    @BindView(R.id.id_detail_contont)
    TextView idDetailContont;
    @BindView(R.id.id_detail_money)
    TextView idDetailMoney;
    @BindView(R.id.id_detail_add)
    TextView idDetailAdd;
    @BindView(R.id.id_detail_now)
    TextView idDetailNow;
    @BindView(R.id.toolbar)
    EnjoyshopToolBar mToolBar;
    private HotGoods.ListBean goodsBean;
    private CartShopProvider cartProvider;

    @Override
    protected void init() {
        //接收数据
        Bundle bundle = getIntent().getExtras();
        goodsBean = (HotGoods.ListBean) bundle.getSerializable("itemClickGoods");
        if (goodsBean == null) {
            finish();
        }
        setData();
        cartProvider = new CartShopProvider(this);

        LogUtil.e("跳转后数据", goodsBean.getName() + goodsBean.getPrice() + goodsBean.getImgUrl(), true);
    }

    private void setData() {
        idDetailTitle.setText(goodsBean.getName());
        idDetailContont.setText(goodsBean.getName());
        GlideUtils.load(this, goodsBean.getImgUrl(), idDetailImg);
        idDetailContont.setText(String.valueOf(goodsBean.getPrice()));

    }

    @Override
    protected int getContentResourseId() {
        return R.layout.activity_detail;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.id_detail_img, R.id.id_detail_add, R.id.id_detail_now, R.id.toolbar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.id_detail_img:
                break;
            case R.id.id_detail_add:
                cartProvider.put(goodsBean);
                ToastUtils.showUiToast(this,"已添加购物车");
                break;
            case R.id.id_detail_now:
                break;
            case R.id.toolbar:
                finish();
                break;
        }
    }
    /**
     * 索尼 （SONY） G9 48英寸全高清 LED液晶电视 （金色）3299.0http://m.360buyimg.com/n4/jfs/t1465/56/1152971182/175914/b39652b4/55e417fcN7d7f9363.jpg!q70.jpg
     */
    /**
     * 初始化标题栏
     */
    private void initToolBar() {


        mToolBar.setRightButtonText("分享");

    }
}
