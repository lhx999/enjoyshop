package com.enjoyshop.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.enjoyshop.R;
import com.enjoyshop.adapter.AdapterDiscount;
import com.enjoyshop.widget.EnjoyshopToolBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 版权：易金卡沃 版权所有
 * <p>
 * 作者：liushijiang
 * <p>
 * 创建日期：2018/11/28
 * <p>
 * 描述：
 * <p>
 * 修订历史：
 */
public class DiscountActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.toolbar)
    EnjoyshopToolBar mToolBar;
    @BindView(R.id.id_dis_recyclerview)
    RecyclerView idDisRecyclerview;
    AdapterDiscount adapterDiscount;

    @Override
    protected void init() {
        initToolBar();
        adapterDiscount = new AdapterDiscount(new ArrayList<String>(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        idDisRecyclerview.setLayoutManager(linearLayoutManager);
        idDisRecyclerview.setAdapter(adapterDiscount);
        getData();
    }

    @Override
    protected int getContentResourseId() {
        return R.layout.activity_discount;
    }

    /**
     * 初始化标题栏
     */
    private void initToolBar() {

        mToolBar.setNavigationOnClickListener(this);
//        mToolBar.setRightButtonText("分享");
        mToolBar.setRightButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.toolbar:
                finish();
                break;
        }
    }

    private void getData() {
        int size = 8;
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add("1");
        }
        adapterDiscount.setNewData(list);
    }
    /**
     * 名称	地点	时间	电话
     * 长春龙嘉国际机场金色驿站	9号登机口对面坐电梯下二楼（安检内）	06:40-21:30	0431-88798697
     */
}
