package com.enjoyshop.appsetting;

import com.tencent.tinker.loader.app.TinkerApplication;
import com.tencent.tinker.loader.shareutil.ShareConstants;

/**
 * 版权：易金卡沃 版权所有
 * <p>
 * 作者：liushijiang
 * <p>
 * 创建日期：2018/9/6
 * <p>
 * 描述：
 * <p>
 * 修订历史：
 */
public class SampleApplication extends TinkerApplication {
    public SampleApplication() {
        super(ShareConstants.TINKER_ENABLE_ALL, "com.enjoyshop.EnjoyshopApplication",
                "com.tencent.tinker.loader.TinkerLoader", false);

    }
}