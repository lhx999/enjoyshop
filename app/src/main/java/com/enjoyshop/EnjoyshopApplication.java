package com.enjoyshop;

import android.annotation.TargetApi;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Environment;
import android.os.Vibrator;
import android.support.multidex.MultiDex;

import com.enjoyshop.bean.User;
import com.enjoyshop.data.dao.DaoMaster;
import com.enjoyshop.data.dao.DaoSession;
import com.enjoyshop.service.LocationService;
import com.enjoyshop.utils.UserLocalData;
import com.enjoyshop.utils.Utils;
import com.mob.MobApplication;
import com.mob.MobSDK;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;
import com.tencent.tinker.loader.app.DefaultApplicationLike;


/**
 * <pre>
 *     author : 高磊华
 *     e-mail : 984992087@qq.com
 *     time   : 2017/08/05
 *     desc   : 整个app的管理
 *     version: 1.0
 * </pre>
 */


public class EnjoyshopApplication extends DefaultApplicationLike {

    //    mob信息    app key:201f8a7a91c30      App Secret:  c63ec5c1eeac1f873ec78c1365120431
    //百度地图的 ak   zbqExff1uz8XyUVn5GbyylomCa0rOkmP

    private User user;
    public LocationService locationService;
    public  Vibrator mVibrator;

    private        DaoMaster.DevOpenHelper mHelper;
    private        SQLiteDatabase          db;
    private        DaoMaster               mDaoMaster;
    private static DaoSession              mDaoSession;

    //整个app的上下文
    public static Context sContext;

    private static EnjoyshopApplication mInstance;

    public EnjoyshopApplication(Application application, int tinkerFlags, boolean tinkerLoadVerifyFlag, long applicationStartElapsedTime, long applicationStartMillisTime, Intent tinkerResultIntent) {
        super(application, tinkerFlags, tinkerLoadVerifyFlag, applicationStartElapsedTime, applicationStartMillisTime, tinkerResultIntent);
    }

    public static EnjoyshopApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        // 通过代码注册你的AppKey和AppSecret
        MobSDK.init(getApplication(), "201f8a7a91c30", "c63ec5c1eeac1f873ec78c1365120431");

        sContext = getApplication();
        initUser();
        Utils.init(getApplication());

        locationService = new LocationService(getApplication());
        mVibrator = (Vibrator) getApplication().getSystemService(Service.VIBRATOR_SERVICE);

        setDatabase();
        MobSDK.init(getApplication(), "201f8a7a91c30", "c63ec5c1eeac1f873ec78c1365120431");
        initBugly();
    }


    private void initUser() {
        this.user = UserLocalData.getUser(getApplication());
    }


    public User getUser() {
        return user;
    }

    public void putUser(User user, String token) {
        this.user = user;
        UserLocalData.putUser(getApplication(), user);
        UserLocalData.putToken(getApplication(), token);
    }

    public void clearUser() {
        this.user = null;
        UserLocalData.clearUser(getApplication());
        UserLocalData.clearToken(getApplication());
    }


    public String getToken() {
        return UserLocalData.getToken(getApplication());
    }

    private Intent intent;

    public void putIntent(Intent intent) {
        this.intent = intent;
    }

    public Intent getIntent() {
        return intent;
    }

    public void jumpToTargetActivity(Context context) {
        context.startActivity(intent);
        this.intent = null;
    }


    public static EnjoyshopApplication getApplication2() {
        return mInstance;
    }

    /**
     * 设置greenDao
     */
    private void setDatabase() {
        // 通过 DaoMaster 的内部类 DevOpenHelper，你可以得到一个便利的 SQLiteOpenHelper 对象。
        // 可能你已经注意到了，你并不需要去编写「CREATE TABLE」这样的 SQL 语句，因为 greenDAO 已经帮你做了。
        // 注意：默认的 DaoMaster.DevOpenHelper 会在数据库升级时，删除所有的表，意味着这将导致数据的丢失。
        // 所以，在正式的项目中，你还应该做一层封装，来实现数据库的安全升级。
        mHelper = new DaoMaster.DevOpenHelper(getApplication(), "shop-db", null);
        db = mHelper.getWritableDatabase();
        // 注意：该数据库连接属于 DaoMaster，所以多个 Session 指的是相同的数据库连接。
        mDaoMaster = new DaoMaster(db);
        mDaoSession = mDaoMaster.newSession();
    }

    public static DaoSession getDaoSession() {
        return mDaoSession;
    }

    public SQLiteDatabase getDb() {
        return db;
    }
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onBaseContextAttached(Context base) {
        super.onBaseContextAttached(base);
        // you must install multiDex whatever tinker is installed!
        MultiDex.install(base);

        // 安装tinker
        // TinkerManager.installTinker(this); 替换成下面Bugly提供的方法
        Beta.installTinker(this);
    }
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void registerActivityLifecycleCallback(Application.ActivityLifecycleCallbacks callbacks) {
        getApplication().registerActivityLifecycleCallbacks(callbacks);
    }
    private void initBugly() {


        Bugly.init(getApplication(), "057a9c6546", true);//
        Beta.autoInit=true;
        Beta.autoCheckUpgrade = true;
        Beta.initDelay=1*1000;
        Beta.storageDir= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        Beta.autoDownloadOnWifi=true;

    }
}
